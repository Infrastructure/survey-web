#!/bin/bash

mkdir -p /var/www/html/tmp/{assets,runtime,upload}
cp /var/www/html/application/config/config.php{.cm,}
cp /var/www/html/application/config/security.php{.cm,}
php application/commands/console.php updatedb
exec php-fpm
